﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FARMACIAMQR
{
	public class RepositorioEmpleados
	{
		ManejadorDeArchivos archivoEmpleados;
		List<Empleado> Empleados;
		public RepositorioEmpleados()
		{
			archivoEmpleados = new ManejadorDeArchivos("Empleados.poo");
			Empleados = new List<Empleado>();
		}

		public bool AgregarEmpleado(Empleado empleado, bool x)
		{
			if (x == false)
			{
				Empleados = LeerEmpleados();
			}
			Empleados.Add(empleado);
			bool resultado = ActualizarArchivo();
			return resultado;
		}

		public bool EliminarEmpleado(Empleado empleado)
		{
			Empleado temporal = new Empleado();
			foreach (var item in Empleados)
			{
				if (item.Nombre == empleado.Nombre)
				{
					temporal = item;
				}
			}
			Empleados.Remove(temporal);
			bool resultado = ActualizarArchivo();
			Empleados = LeerEmpleados();
			return resultado;
		}


		private bool ActualizarArchivo()
		{
			string datos = "";
			foreach (Empleado item in Empleados)
			{
				datos += string.Format("{0}|{1}|{2}\n", item.Nombre, item.Puesto, item.Telefono);
			}
			return archivoEmpleados.Guardar(datos);
		}
		public List<Empleado> LeerEmpleados()
		{
			string datos = archivoEmpleados.Leer();
			if (datos != null)
			{
				List<Empleado> empleado = new List<Empleado>();
				string[] lineas = datos.Split('\n');
				for (int i = 0; i < lineas.Length - 1; i++)
				{
					string[] campos = lineas[i].Split('|');
					Empleado a = new Empleado()
					{
						Nombre = campos[0],
						Puesto = campos[1],
						Telefono = campos[2],
					};
					empleado.Add(a);
				}
				Empleados = empleado;
				return empleado;
			}
			else
			{
				return null;
			}
		}
	}
}
