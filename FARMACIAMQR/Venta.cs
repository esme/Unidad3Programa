﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FARMACIAMQR
{
	public class Venta
	{
		public string FechaVenta { get; set; }
		public List<int> Cantidad { get; set; }
		public List<Medicamento> Productos { get; set; }
		public Cliente Cliente { get; set; }
		public Empleado Vendedor { get; set; }

	}
}
