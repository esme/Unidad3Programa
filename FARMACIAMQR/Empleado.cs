﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FARMACIAMQR
{
	public class Empleado:Persona
	{
		public string Puesto { get; set; }
		public override string ToString()
		{
			return string.Format("{0}", Nombre);
		}
	}

}
