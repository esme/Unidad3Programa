﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FARMACIAMQR
{
	public class RepositorioCategoria
	{
		ManejadorDeArchivos archivoCategoria;
		List<Categoria> Categorias;
		public RepositorioCategoria()
		{
			archivoCategoria = new ManejadorDeArchivos("Categorias.poo");
			Categorias = new List<Categoria>();
		}

		public bool AgregarCategoria(Categoria categoria,bool x)
		{
			if (x==false)
			{
				Categorias = LeerCategorias();
			}
			Categorias.Add(categoria);
			bool resultado = ActualizarArchivo();
			return resultado;
		}

		public bool EliminarCategoria(Categoria categoria)
		{
			Categoria temporal = new Categoria();
			foreach (var item in Categorias)
			{
				if (item.Nombre == categoria.Nombre)
				{
					temporal = item;
				}
			}
			Categorias.Remove(temporal);
			bool resultado = ActualizarArchivo();
			Categorias = LeerCategorias();
			return resultado;
		}


		private bool ActualizarArchivo()
		{
			string datos = "";
			foreach (Categoria item in Categorias)
			{
				datos += string.Format("{0}|{1}\n", item.Nombre, item.Descripcion);
			}
			return archivoCategoria.Guardar(datos);
		}
		public List<Categoria> LeerCategorias()
		{
			string datos = archivoCategoria.Leer();
			if (datos != null)
			{
				List<Categoria> categoria = new List<Categoria>();
				string[] lineas = datos.Split('\n');
				for (int i = 0; i < lineas.Length - 1; i++)
				{
					string[] campos = lineas[i].Split('|');
					Categoria a = new Categoria()
					{
						Nombre = campos[0],
						Descripcion = campos[1],
					};
					categoria.Add(a);
				}
				Categorias = categoria;
				return categoria;
			}
			else
			{
				return null;
			}
		}
	}
}
