﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FARMACIAMQR
{
	public class RepositorioClientes
	{
		ManejadorDeArchivos archivoClientes;
		List<Cliente> Clientes;
		public RepositorioClientes()
		{
			archivoClientes = new ManejadorDeArchivos("Clientes.poo");
			Clientes = new List<Cliente>();
		}

		public bool AgregarCliente(Cliente cliente, bool x)
		{
			if (x == false)
			{
				Clientes = LeerClientes();
			}
			Clientes.Add(cliente);
			bool resultado = ActualizarArchivo();
			return resultado;
		}

		public bool EliminarCliente(Cliente cliente)
		{
			Cliente temporal = new Cliente();
			foreach (var item in Clientes)
			{
				if (item.Nombre == cliente.Nombre)
				{
					temporal = item;
				}
			}
			Clientes.Remove(temporal);
			bool resultado = ActualizarArchivo();
			Clientes = LeerClientes();
			return resultado;
		}


		private bool ActualizarArchivo()
		{
			string datos = "";
			foreach (Cliente item in Clientes)
			{
				datos += string.Format("{0}|{1}|{2}|{3}|{4}\n", item.Nombre, item.RFC,item.Direccion,item.Telefono,item.Correo);
			}
			return archivoClientes.Guardar(datos);
		}
		public List<Cliente> LeerClientes()
		{
			string datos = archivoClientes.Leer();
			if (datos != null)
			{
				List<Cliente> cliente = new List<Cliente>();
				string[] lineas = datos.Split('\n');
				for (int i = 0; i < lineas.Length - 1; i++)
				{
					string[] campos = lineas[i].Split('|');
					Cliente a = new Cliente()
					{
						Nombre = campos[0],
						RFC = campos[1],
						Direccion = campos[2],
						Telefono = campos[3],
						Correo = campos[4],
					};
					cliente.Add(a);
				}
				Clientes = cliente;
				return cliente;
			}
			else
			{
				return null;
			}
		}
	}
}
