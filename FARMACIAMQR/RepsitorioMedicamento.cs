﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FARMACIAMQR
{
	public class RepsitorioMedicamento
	{
		ManejadorDeArchivos archivoMedicamentos;
		List<Medicamento> Medicamentos;
		public RepsitorioMedicamento()
		{
			archivoMedicamentos = new ManejadorDeArchivos("Medicamentos.poo");
			Medicamentos = new List<Medicamento>();
		}

		public bool AgregarMedicamento(Medicamento medicina,bool x)
		{
			if (x == false)
			{
				Medicamentos = LeerMedicamentos();
			}
			Medicamentos.Add(medicina);
			bool resultado = ActualizarArchivo();
			return resultado;
		}

		public bool EliminarMedicamento(Medicamento medicina)
		{
			Medicamento temporal = new Medicamento();
			foreach (var item in Medicamentos)
			{
				if (item.Nombre == medicina.Nombre)
				{
					temporal = item;
				}
			}
			Medicamentos.Remove(temporal);
			bool resultado = ActualizarArchivo();
			Medicamentos = LeerMedicamentos();
			return resultado;
		}

		
		private bool ActualizarArchivo()
		{
			string datos = "";
			foreach (Medicamento item in Medicamentos)
			{
				datos += string.Format("{0}|{1}|{2}|{3}|{4}|{5}\n", item.Nombre, item.PrecioCompra, item.PrecioVenta,item.Categoria.Nombre,item.Presentacion, item.Descripcion);
			}
			return archivoMedicamentos.Guardar(datos);
		}
		public List<Medicamento> LeerMedicamentos()
		{
			string datos = archivoMedicamentos.Leer();
			if (datos != null)
			{
				List<Medicamento> medicamentos = new List<Medicamento>();
				string[] lineas = datos.Split('\n');
				for (int i = 0; i < lineas.Length - 1; i++)
				{
					string[] campos = lineas[i].Split('|');
					Medicamento a = new Medicamento()
					{
						Nombre = campos[0],
						PrecioCompra = campos[1],
						PrecioVenta = campos[2],
						Categoria = new Categoria()
						{
							Nombre = campos[3],
						},
						Descripcion = campos[4],
						Presentacion = campos[5],
					};
					medicamentos.Add(a);
				}
				Medicamentos = medicamentos;
				return medicamentos;
			}
			else
			{
				return null;
			}
		}
	}
}
