﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FARMACIAMQR
{
	public class Medicamento
	{
		public string Nombre { get; set; }
		public string PrecioCompra { get; set; }
		public string PrecioVenta { get; set; }
		public string Descripcion { get; set; }
		public Categoria Categoria { get; set; }
		public string Presentacion { get; set; }

	}
}
