﻿using FARMACIAMQR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ui
{
    /// <summary>
    /// Lógica de interacción para NuevoCliente.xaml
    /// </summary>
    public partial class NuevoCliente : Window

    {
		bool Editar;
		Cliente Original;
		RepositorioClientes repositorioCliente;
		public NuevoCliente(Cliente c, bool editar)
        {
            InitializeComponent();
			repositorioCliente = new RepositorioClientes();
			txbDireccion.Clear();
			txbNombreEmpleado.Clear();
			txbemail.Clear();
			txbRFC.Clear();
			txbTelefono.Clear();
			if (editar == true)
			{
				txbDireccion.Text = c.Direccion;
				txbNombreEmpleado.Text = c.Nombre;
				txbemail.Text = c.Correo;
				txbRFC.Text = c.RFC;
				txbTelefono.Text = c.Telefono;
			}
			Original = c;
			Editar = editar;
		}


		private void btnLimpiar_Click(object sender, RoutedEventArgs e)
		{
			txbDireccion.Clear();
			txbNombreEmpleado.Clear();
			txbemail.Clear();
			txbRFC.Clear();
			txbTelefono.Clear();
		}

		private void btnCancelar_Click(object sender, RoutedEventArgs e)
		{
			VentanaClientes Principal = new VentanaClientes();
			Principal.Show();
			Close();
		}

		private void btnGuardarCliente_Click(object sender, RoutedEventArgs e)
		{
			if (string.IsNullOrEmpty(txbNombreEmpleado.Text) || string.IsNullOrEmpty(txbDireccion.Text)|| string.IsNullOrEmpty(txbRFC.Text)|| string.IsNullOrEmpty(txbTelefono.Text)|| string.IsNullOrEmpty(txbemail.Text))
			{
				MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
				return;
			}
			if (Editar == false)
			{
				Cliente c = new Cliente()
				{
					Nombre = txbNombreEmpleado.Text,
					RFC=txbRFC.Text,
					Direccion=txbDireccion.Text,
					Telefono=txbTelefono.Text,
					Correo=txbemail.Text,
					
				};
				bool x = false;
				if (repositorioCliente.LeerClientes() == null)
				{
					x = true;
				}
				if (repositorioCliente.AgregarCliente(c, x))
				{
					MessageBox.Show("Nueva Cliente Guardado con Éxito", "Categorias", MessageBoxButton.OK, MessageBoxImage.Information);
					VentanaClientes Principal = new VentanaClientes();
					Principal.Show();
					Close();
				}
				else
				{
					MessageBox.Show("Error al guardar al nuevo Cliente,", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}
			else
			{

				Cliente c = new Cliente()
				{
					Nombre = txbNombreEmpleado.Text,
					RFC = txbRFC.Text,
					Direccion = txbDireccion.Text,
					Telefono = txbTelefono.Text,
					Correo = txbemail.Text,
				};
				bool x = false;
				if (repositorioCliente.LeerClientes() == null)
				{
					x = true;
				}
				if (repositorioCliente.AgregarCliente(c, x))

				{
					repositorioCliente.EliminarCliente(Original);
					MessageBox.Show("El cliente ah sido actualizado", "Editar", MessageBoxButton.OK, MessageBoxImage.Information);
					VentanaClientes Principal = new VentanaClientes();
					Principal.Show();
					Close();
				}
				else
				{
					MessageBox.Show("Error al guardar el nuevo cliente", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}



		}
	}
}
