﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FARMACIAMQR;

namespace ui
{
	/// <summary>
	/// Lógica de interacción para VentanaClientes.xaml
	/// </summary>
	public partial class VentanaClientes : Window
	{
		RepsitorioMedicamento repositorioMedicamentos;
		RepositorioClientes repositorioClientes;
		RepositorioCategoria repositorioCategorias;
		RepositorioEmpleados repositorioEmpleados;
		bool editar = false;
		public VentanaClientes()
		{
			InitializeComponent();
			repositorioCategorias = new RepositorioCategoria();
			repositorioMedicamentos = new RepsitorioMedicamento();
			repositorioClientes = new RepositorioClientes();
			repositorioEmpleados= new RepositorioEmpleados();
			cmbCatalogos.Items.Add("Medicamentos");
			cmbCatalogos.Items.Add("Clientes");
			cmbCatalogos.Items.Add("Empleados");
			cmbCatalogos.Items.Add("Categorías");
			btnNuevoElemento.IsEnabled = false;
			btnEditar.IsEnabled = false;
			btnEliminar.IsEnabled = false;
			dtgEmpleados.ItemsSource = null;
		}
		private void btnRegresar_Click(object sender, RoutedEventArgs e)
		{
			MainWindow Principal = new MainWindow();
			Principal.Show();
			Close();
		}

		private void btnVerCatologo_Click(object sender, RoutedEventArgs e)
		{
			if (cmbCatalogos.Text == "Medicamentos")
			{
				dtgEmpleados.ItemsSource = repositorioMedicamentos.LeerMedicamentos();
			}
			if (cmbCatalogos.Text == "Categorías")
			{
				dtgEmpleados.ItemsSource = null;
				dtgEmpleados.ItemsSource = repositorioCategorias.LeerCategorias();
			}
			if (cmbCatalogos.Text == "Clientes")
			{
				dtgEmpleados.ItemsSource = null;
				dtgEmpleados.ItemsSource = repositorioClientes.LeerClientes();
			}
			if (cmbCatalogos.Text == "Empleados")
			{
				dtgEmpleados.ItemsSource = null;
				dtgEmpleados.ItemsSource = repositorioEmpleados.LeerEmpleados();
			}
			btnNuevoElemento.IsEnabled = true;
			btnEditar.IsEnabled = true;
			btnEliminar.IsEnabled = true;
		}

		private void btnNuevoElemento_Click(object sender, RoutedEventArgs e)
		{
			if (cmbCatalogos.Text == "Medicamentos")
			{
				Medicamento c = new Medicamento();
				VentanaMedicamento NuevoMedicamento = new VentanaMedicamento(c, editar);
				NuevoMedicamento.Show();
				Close();
			}
			if (cmbCatalogos.Text == "Empleados")
			{
				Empleado c = new Empleado();
				NuevoEmpleado NuevoMedicamento = new NuevoEmpleado(c, editar);
				NuevoMedicamento.Show();
				Close();
			}
			if (cmbCatalogos.Text == "Categorías")
			{
				Categoria c = new Categoria();
				VentanaCategorias NuevoMedicamento = new VentanaCategorias(c,editar);
				NuevoMedicamento.Show();
				Close();
			}
			if (cmbCatalogos.Text == "Clientes")
			{
				Cliente c = new Cliente();
				NuevoCliente NuevoCliente = new NuevoCliente(c, editar);
				NuevoCliente.Show();
				Close();
			}
			

		}

		private void btnEliminar_Click(object sender, RoutedEventArgs e)
		{
			if (cmbCatalogos.Text == "Categorías")
			{
				EliminarCategoria();
			}
			if (cmbCatalogos.Text == "Medicamentos")
			{
				EliminarMedicamento();
			}
			if (cmbCatalogos.Text == "Clientes")
			{
				EliminarCliente();
			}
			if (cmbCatalogos.Text == "Empleados")
			{
				EliminarEmpleado();
			}

		}

		private void btnEditar_Click(object sender, RoutedEventArgs e)
		{
			if (cmbCatalogos.Text == "Categorías")
			{
				EditarCategoria();
			}
			if (cmbCatalogos.Text == "Medicamentos")
			{
				EditarMedicamento();
			}
			if (cmbCatalogos.Text == "Clientes")
			{
				EditarCliente();
			}
			if (cmbCatalogos.Text == "Empleados")
			{
				EditarEmpelado();
			}

		}
		private void EditarEmpelado()
		{
			if (repositorioEmpleados.LeerEmpleados().Count == 0)
			{
				MessageBox.Show("No existen elementos para modificar", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			else
			{
				if (dtgEmpleados.SelectedItem != null)
				{
					Empleado c = dtgEmpleados.SelectedItem as Empleado;
					editar = true;
					NuevoEmpleado NuevoMedicamento = new NuevoEmpleado(c, editar);
					NuevoMedicamento.Show();
					Close();

				}
				else
				{
					MessageBox.Show("No existe seleccion", "Verifique", MessageBoxButton.OK, MessageBoxImage.Question);
				}
			}
		}

		private void EditarCliente()
		{
			if (repositorioClientes.LeerClientes().Count == 0)
			{
				MessageBox.Show("No existen elementos para modificar", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			else
			{
				if (dtgEmpleados.SelectedItem != null)
				{
					Cliente c = dtgEmpleados.SelectedItem as Cliente;
					editar = true;
					NuevoCliente NuevoMedicamento = new NuevoCliente(c, editar);
					NuevoMedicamento.Show();
					Close();

				}
				else
				{
					MessageBox.Show("No existe seleccion", "Verifique", MessageBoxButton.OK, MessageBoxImage.Question);
				}
			}
		}


		private void EditarCategoria()
		{
			if (repositorioCategorias.LeerCategorias().Count == 0)
			{
				MessageBox.Show("No existen elementos para modificar", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			else
			{
				if (dtgEmpleados.SelectedItem != null)
				{
					Categoria c = dtgEmpleados.SelectedItem as Categoria;
					editar = true;
					VentanaCategorias NuevoMedicamento = new VentanaCategorias(c, editar);
					NuevoMedicamento.Show();
					Close();

				}
				else
				{
					MessageBox.Show("No existe seleccion", "Verifique", MessageBoxButton.OK, MessageBoxImage.Question);
				}
			}

		}
		private void EditarMedicamento()
		{
			if (repositorioMedicamentos.LeerMedicamentos().Count == 0)
			{
				MessageBox.Show("No existen elementos para modificar", "Advertencia", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			else
			{
				if (dtgEmpleados.SelectedItem != null)
				{
					Medicamento c = dtgEmpleados.SelectedItem as Medicamento;
					editar = true;
					VentanaMedicamento NuevoMedicamento = new VentanaMedicamento(c, editar);
					NuevoMedicamento.Show();
					Close();

				}
				else
				{
					MessageBox.Show("No existe seleccion", "Verifique", MessageBoxButton.OK, MessageBoxImage.Question);
				}
			}

		}
		private void EliminarEmpleado()
		{

			if (repositorioEmpleados.LeerEmpleados().Count == 0)
			{
				MessageBox.Show("Aun no existen Empleados", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			else
			{
				if (dtgEmpleados.SelectedItem != null)
				{
					Empleado c = dtgEmpleados.SelectedItem as Empleado;
					if (MessageBox.Show("Realmente desea eliminar al empleado " + c.Nombre + "?", "Aviso", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
					{
						if (repositorioEmpleados.EliminarEmpleado(c))
						{
							MessageBox.Show("El empleado " + c.Nombre + " ah sido eliminado", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
							dtgEmpleados.ItemsSource = null;
							dtgEmpleados.ItemsSource = repositorioEmpleados.LeerEmpleados();
						}
						else
						{
							MessageBox.Show("Error al eliminar al empleado", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
						}
					}
				}
				else
				{
					MessageBox.Show("Seleccione el elemento que desea eliminar", "Indefinido", MessageBoxButton.OK, MessageBoxImage.Question);
				}
			}
		}
		private void EliminarCategoria()
		{
			if (repositorioCategorias.LeerCategorias().Count == 0)
			{
				MessageBox.Show("Aun no existen Categorias", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			else
			{
				if (dtgEmpleados.SelectedItem != null)
				{
					Categoria c = dtgEmpleados.SelectedItem as Categoria;
					if (MessageBox.Show("Realmente desea eliminar la Categoria " + c.Nombre + "?", "Aviso", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
					{
						if (repositorioCategorias.EliminarCategoria(c))
						{
							MessageBox.Show("La categoria " + c.Nombre + " ah sido eliminada", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
							dtgEmpleados.ItemsSource = null;
							dtgEmpleados.ItemsSource = repositorioCategorias.LeerCategorias();
						}
						else
						{
							MessageBox.Show("Error al eliminar la categoria", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
						}
					}
				}
				else
				{
					MessageBox.Show("Seleccione el elemento que desea eliminar", "Indefinido", MessageBoxButton.OK, MessageBoxImage.Question);
				}
			}
		}
		private void EliminarMedicamento()
		{
			if (repositorioMedicamentos.LeerMedicamentos().Count == 0)
			{
				MessageBox.Show("Aun no existen Medicamentos", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			else
			{
				if (dtgEmpleados.SelectedItem != null)
				{
					Medicamento m = dtgEmpleados.SelectedItem as Medicamento;
					if (MessageBox.Show("Realmente desea eliminar El Medicamento " + m.Nombre + "?", "Aviso", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
					{
						if (repositorioMedicamentos.EliminarMedicamento(m))
						{
							MessageBox.Show("El Medicamento " + m.Nombre + " ah sido eliminada", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
							dtgEmpleados.ItemsSource = null;
							dtgEmpleados.ItemsSource = repositorioMedicamentos.LeerMedicamentos();
						}
						else
						{
							MessageBox.Show("Error al eliminar el Medicamentos", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
						}
					}
				}
				else
				{
					MessageBox.Show("Seleccione el elemento que desea eliminar", "Indefinido", MessageBoxButton.OK, MessageBoxImage.Question);
				}
			}
		}
		private void EliminarCliente()
		{
			if (repositorioClientes.LeerClientes().Count == 0)
			{
				MessageBox.Show("Aun no existen Clientes", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			else
			{
				if (dtgEmpleados.SelectedItem != null)
				{
					Cliente m = dtgEmpleados.SelectedItem as Cliente;
					if (MessageBox.Show("Realmente desea eliminar El Cliente " + m.Nombre + "?", "Aviso", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
					{
						if (repositorioClientes.EliminarCliente(m))
						{
							MessageBox.Show("El Cliente " + m.Nombre + " ah sido eliminado", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
							dtgEmpleados.ItemsSource = null;
							dtgEmpleados.ItemsSource = repositorioClientes.LeerClientes();
						}
						else
						{
							MessageBox.Show("Error al eliminar el Cliente", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
						}
					}
				}
				else
				{
					MessageBox.Show("Seleccione el elemento que desea eliminar", "Indefinido", MessageBoxButton.OK, MessageBoxImage.Question);
				}
			}
		}
	}
}
