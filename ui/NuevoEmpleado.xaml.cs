﻿using FARMACIAMQR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ui
{
	/// <summary>
	/// Lógica de interacción para NuevoEmpleado.xaml
	/// </summary>
	public partial class NuevoEmpleado : Window
	{
		bool Editar;
		Empleado Original;
		RepositorioEmpleados repositorioEmpleados;
		public NuevoEmpleado(Empleado c, bool editar)
		{
			InitializeComponent();
			repositorioEmpleados = new RepositorioEmpleados();
			txbNombreEmpleado.Clear();
			txbPuesto.Clear();
			txbTelefono.Clear();
			if (editar == true)
			{
				txbNombreEmpleado.Text = c.Nombre;
				txbPuesto.Text = c.Puesto;
				txbTelefono.Text = c.Telefono;

			}
			Original = c;
			Editar = editar;
		}

		private void btnLimpiar_Click(object sender, RoutedEventArgs e)
		{
			txbNombreEmpleado.Clear();
			txbPuesto.Clear();
			txbTelefono.Clear();
		}

		private void btnCancelar_Click(object sender, RoutedEventArgs e)
		{
			VentanaClientes Principal = new VentanaClientes();
			Principal.Show();
			Close();

		}

		private void btnGuardarEmpleado_Click(object sender, RoutedEventArgs e)
		{
			if (string.IsNullOrEmpty(txbNombreEmpleado.Text) || string.IsNullOrEmpty(txbPuesto.Text) || string.IsNullOrEmpty(txbTelefono.Text))
			{
				MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
				return;
			}
			if (Editar == false)
			{
				Empleado c = new Empleado()
				{
					Nombre = txbNombreEmpleado.Text,
					Puesto = txbPuesto.Text,
					Telefono=txbTelefono.Text,
				};
				bool x = false;
				if (repositorioEmpleados.LeerEmpleados() == null)
				{
					x = true;
				}
				if (repositorioEmpleados.AgregarEmpleado(c, x))
				{
					MessageBox.Show("Nuevo Empleado Guardada con Éxito", "Empleados", MessageBoxButton.OK, MessageBoxImage.Information);
					VentanaClientes Principal = new VentanaClientes();
					Principal.Show();
					Close();
				}
				else
				{
					MessageBox.Show("Error al guardar el nuevo empleado,", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}
			else
			{

				Empleado c = new Empleado()
				{
					Nombre = txbNombreEmpleado.Text,
					Puesto = txbPuesto.Text,
					Telefono = txbTelefono.Text,
				};
				bool x = false;
				if (repositorioEmpleados.LeerEmpleados() == null)
				{
					x = true;
				}
				if (repositorioEmpleados.AgregarEmpleado(c, x))

				{
					repositorioEmpleados.EliminarEmpleado(Original);
					MessageBox.Show("El empleado ah sido actualizado", "Editar", MessageBoxButton.OK, MessageBoxImage.Information);
					VentanaClientes Principal = new VentanaClientes();
					Principal.Show();
					Close();
				}
				else
				{
					MessageBox.Show("Error al guardar al nuevo empleado", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}

		}
	}
}
