﻿using FARMACIAMQR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ui
{
    /// <summary>
    /// Lógica de interacción para VentanaCategorias.xaml
    /// </summary>
    public partial class VentanaCategorias : Window

    {
		bool Editar;
		Categoria Original;
		RepositorioCategoria repositorioCategorias;
		public VentanaCategorias(Categoria c, bool editar)
        {
            InitializeComponent();
			repositorioCategorias = new RepositorioCategoria();
			txbDescripcion.Clear();
			txbNombreCategoria.Clear();
			if (editar == true)
			{
				txbNombreCategoria.Text = c.Nombre;
				txbDescripcion.Text = c.Descripcion;
			}
			Original = c;
			Editar = editar;
		
		}

		private void btnCancelar_Click(object sender, RoutedEventArgs e)
		{
			VentanaClientes Principal = new VentanaClientes();
			Principal.Show();
			Close();
		}

		private void btnLimpiar_Click(object sender, RoutedEventArgs e)
		{
			txbDescripcion.Clear();
			txbNombreCategoria.Clear();
		}

		private void btnGuardarCat_Click(object sender, RoutedEventArgs e)
		{
			if (string.IsNullOrEmpty(txbNombreCategoria.Text) || string.IsNullOrEmpty(txbDescripcion.Text))
			{
				MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
				return;
			}
			if (Editar == false) {
				Categoria c = new Categoria()
				{
					Nombre = txbNombreCategoria.Text,
					Descripcion = txbDescripcion.Text,
				};
				bool x = false;
				if (repositorioCategorias.LeerCategorias() == null)
				{
					x = true;
				}
				if (repositorioCategorias.AgregarCategoria(c, x))
				{
					MessageBox.Show("Nueva Categoria Guardada con Éxito", "Categorias", MessageBoxButton.OK, MessageBoxImage.Information);
					VentanaClientes Principal = new VentanaClientes();
					Principal.Show();
					Close();
				}
				else
				{
					MessageBox.Show("Error al guardar la nueva Categoria,", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}
			else
			{

				Categoria c = new Categoria()
				{
					Nombre = txbNombreCategoria.Text,
					Descripcion = txbDescripcion.Text,
				};
				bool x = false;
				if (repositorioCategorias.LeerCategorias() == null)
				{
					x = true;
				}
				if (repositorioCategorias.AgregarCategoria(c,x))

				{
					repositorioCategorias.EliminarCategoria(Original);
					MessageBox.Show("La categoria ah sido actualizada", "Editar", MessageBoxButton.OK, MessageBoxImage.Information);
					VentanaClientes Principal = new VentanaClientes();
					Principal.Show();
					Close();
				}
				else
				{
					MessageBox.Show("Error al guardar la nueva Categoria", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}


		}
	}
}
