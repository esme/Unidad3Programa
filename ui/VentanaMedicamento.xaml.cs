﻿using FARMACIAMQR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ui
{
    /// <summary>
    /// Lógica de interacción para VentanaMedicamento.xaml
    /// </summary>
    public partial class VentanaMedicamento : Window
    {
		RepsitorioMedicamento repositorioMedicamentos;
		RepositorioCategoria repositorioCategorias;
		bool Editar= false;
		Medicamento Original;
		public VentanaMedicamento(Medicamento c, bool editar)
        {
            InitializeComponent();
			repositorioCategorias = new RepositorioCategoria();
			repositorioMedicamentos = new RepsitorioMedicamento();
			cmbCategorias.ItemsSource = repositorioCategorias.LeerCategorias();
			txbDescripcion.Clear();
			txbNombreMedicamento.Clear();
			txbPrecioCompra.Clear();
			txbPrecioVenta.Clear();
			txbPresentacion.Clear();
			if (editar == true)
			{
				txbNombreMedicamento.Text = c.Nombre;
				txbDescripcion.Text = c.Descripcion;
				txbPrecioCompra.Text = c.PrecioCompra;
				txbPrecioVenta.Text = c.PrecioVenta;
				txbPresentacion.Text = c.Presentacion;
			}
			Original = c;
			Editar = editar;
		}

		private void btnGuardar_Click(object sender, RoutedEventArgs e)
		{
			if (string.IsNullOrEmpty(txbNombreMedicamento.Text) || string.IsNullOrEmpty(txbPrecioCompra.Text) || string.IsNullOrEmpty(txbPrecioVenta.Text) || string.IsNullOrEmpty(txbPresentacion.Text) || string.IsNullOrEmpty(txbDescripcion.Text) || cmbCategorias.SelectedIndex == -1)
			{
				MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
				return;
			}
			if (Editar == false)
			{
				Medicamento m = new Medicamento()
			{
				Nombre = txbNombreMedicamento.Text,
				Descripcion = txbDescripcion.Text,
				PrecioCompra=txbPrecioCompra.Text,
				PrecioVenta=txbPrecioVenta.Text,
				Presentacion=txbPresentacion.Text,
				Categoria = new Categoria()
				{
					Nombre = cmbCategorias.Text,
				},
			};
			bool x = false;
			if (repositorioCategorias.LeerCategorias() == null)
			{
				x = true;
			}
			if (repositorioMedicamentos.AgregarMedicamento(m,x))
			{
				MessageBox.Show("Nueva Medicamento Guardada con Éxito", "Medicamento", MessageBoxButton.OK, MessageBoxImage.Information);
				VentanaClientes Principal = new VentanaClientes();
				Principal.Show();
				Close();
			}
			else
			{
				MessageBox.Show("Error al guardar el nuevo Medicamento,", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			}
			else
			{

				Medicamento c = new Medicamento()
				{
					Nombre = txbNombreMedicamento.Text,
					Descripcion = txbDescripcion.Text,
					PrecioCompra = txbPrecioCompra.Text,
					PrecioVenta = txbPrecioVenta.Text,
					Presentacion = txbPresentacion.Text,
					Categoria = new Categoria()
					{
						Nombre = cmbCategorias.Text,
					},
				};
				bool x = false;
				if (repositorioMedicamentos == null)
				{
					x = true;
				}
				if (repositorioMedicamentos.AgregarMedicamento(c, x))
				{
					repositorioMedicamentos.EliminarMedicamento(Original);
					MessageBox.Show("El medicamento ah sido actualizada", "Editar", MessageBoxButton.OK, MessageBoxImage.Information);
					VentanaClientes Principal = new VentanaClientes();
					Principal.Show();
					Close();
				}
				else
				{
					MessageBox.Show("Error al guardar el nuevo Medicamento", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}


		}

		private void btnSeleccionar_Click(object sender, RoutedEventArgs e)
		{
			cmbCategorias.IsEnabled = false;
		}

		private void btnLimpiar_Click(object sender, RoutedEventArgs e)
		{
			txbDescripcion.Clear();
			txbNombreMedicamento.Clear();
			txbPrecioCompra.Clear();
			txbPrecioVenta.Clear();
			txbPresentacion.Clear();
			cmbCategorias.IsEnabled = true;
		}

		private void btnCancelar_Click(object sender, RoutedEventArgs e)
		{
			VentanaClientes Principal = new VentanaClientes();
			Principal.Show();
			Close();
		}
	}
}
